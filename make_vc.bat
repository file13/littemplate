
@echo off
cd build
conan install .. --build=missing
cmake .. -G "Visual Studio 15" -A x64
cmake --build . --config Release
cd ..
