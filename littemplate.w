% This is a nuweb literate program: littemplate.w

% Then to tangle/weave:
% nuweb -rl littemplate.w

% To modify this for a new project, see the README after tangling.

% To make unix autotools:
% autoreconf -iv; ./configure --enable-debug; make check

% To make unix autotools with the PDF:
% autoreconf -iv; ./configure --enable-debug; make tangle-all check


\documentclass[10pt]{article}
%\documentclass[a4paper]{report}
%\documentclass[9pt]{extarticle}
\usepackage[utf8]{inputenc}

\usepackage{latexsym}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{courier}
\usepackage{indentfirst}

% Code styles
\usepackage{ascii}
\usepackage{listings}
\usepackage[dvipsnames]{xcolor}

% Custom Colors if you want to tweak colors
%\definecolor{parchment}{RGB}{249, 232, 209} % darker
\definecolor{parchment}{RGB}{255, 250, 243} % lighter

% If we want to change the background of the whole document
%\usepackage{pagecolor,lipsum}
%\pagecolor{parchment}

% Extra Keywords: I need to figure out way to make keywords reusable.
%\newcommand{\extrakeywords}{guint, GString, GPtrArray}

% Use this style for plain lstlisting C.
\lstdefinestyle{Mono C}{
  extendedchars=true,keepspaces=true,language=C,
  basicstyle=\footnotesize\ttfamily,
  % To create more custom keywords--for example, typedefs, add them
  % to morekeywords.
  morekeywords={guint, GString, GPtrArray},
}

% Use this style for plain lstlisting C with color.
\lstdefinestyle{Mono Color C}{
  extendedchars=true,keepspaces=true,language=C,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{RoyalBlue},
  identifierstyle=\bfseries,
  commentstyle=\itshape\color{BrickRed},
  stringstyle=\color{OliveGreen},
  % To create more custom keywords--for example, typedefs, add them
  % to morekeywords.
  morekeywords={guint, GString, GPtrArray},
}

% Use this style for CWEB style code formatting.
% Feel free to change any of this to suit personal preference.
\lstdefinestyle{CWEB C}{
  extendedchars=true,keepspaces=true,language=C,
  basicstyle=\normalsize\rmfamily,
  identifierstyle=\itshape,
  commentstyle=\normalsize\rmfamily,
  stringstyle=\normalsize\asciifamily,
  showstringspaces=true,
  % To create more custom keywords--for example, typedefs, add them
  % to morekeywords.
  %morekeywords={\extrakeywords},
  morekeywords={guint, GString, GPtrArray},
  literate=*
  % General CWEB Style
  {+}{{$+$}}1
  {-}{{$-$}}1
  {*}{{$*$}}1
  {(}{{$($}}1 {)}{{$)$}}1
  {<}{{$<$}}1 {>}{{$>$}}1
  {<=}{{$\leq$}}1 {>=}{{$\geq$}}1 
  %{!}{{\kern.3em{$\lnot$}}}2
  {!}{{$\lnot$}}2
  {!=}{{$\not=$}}2
  {||}{{$\lor$}}2
  {&&}{{$\land$}}2
  %{|}{{$\neg$}}1
  {\%}{{\footnotesize{$\%$}}}1
  {^}{{$\oplus$}}1
  {~}{{$\sim$}}1
  {<<}{{$\ll$}}1
  {>>}{{$\gg$}}1
  {++}{{$+\!+$}}1
  {--}{{$-\kern-.5pt-$}}1
  %{=}{{$\gets$}}1
  {==}{{$\equiv$}}1
  {NULL}{{$\Lambda$}}1
  {->}{{\tiny\raisebox{.6ex}{$\!\rightarrow$}}}1
}

%% To change the C code style if you don't want fancy CWEB style,
%% change it here.
\lstset{style=CWEB C}
%\lstset{style=Mono C}
%\lstset{style=Mono Color C}

\lstdefinestyle{Make}{
  extendedchars=true,keepspaces=true,language=make,
  basicstyle=\footnotesize\ttfamily,
}


% "Example" style for lstlistings, turns off anything fancy
% for when you want to show examples
\lstdefinestyle{example}{
  language=,
  identifierstyle=\normalsize\ttfamily,
  %identifierstyle=\smallsize\courier,
  frame=single,extendedchars=true,
  literate=*
  %{-}{{-}}1 % listing package screws up, puts minus sign for dash 
  %{-}{{$-$}}1
  %{-}{{--}}1 % this interfers with our morekeywords
}

% taken from nuweb
\usepackage{color}
\definecolor{linkcolor}{rgb}{0, 0, 0.7}
%\usepackage[plainpages=false,pdftex,colorlinks,backref]{hyperref}

\usepackage[%
backref,%
raiselinks,%
pdfhighlight=/O,%
pagebackref,%
hyperfigures,%
%breaklinks,%
%plainpages=false,
colorlinks,%
pdfpagemode=UseNone,%
pdfstartview=FitBH,%
linkcolor={linkcolor},%
anchorcolor={linkcolor},%
citecolor={linkcolor},%
filecolor={linkcolor},%
menucolor={linkcolor},%
urlcolor={linkcolor}%
]{hyperref}

%\usepackage[hidelinks]{hyperref} %hide ugly links
\usepackage[margin=1in]{geometry} %smaller margin
\usepackage{fancyvrb} % for our includetextfile
\usepackage{multicol} % for three columns

% Used to include raw text files.
\newcommand{\includetextfile}[1]{
  \VerbatimInput[baselinestretch=1,fontsize=\footnotesize]
  {#1}
}

% Used to include line numbered raw text files.
\newcommand{\includenumberedtextfile}[1]{
  \VerbatimInput[
  baselinestretch=1,
  fontsize=\footnotesize,
  numbers=left]
  {#1}
}

% Used to mark places which should be changed.
\newcommand\boldred[1]{\textcolor{red}{\textbf{#1}}}

\begin{document}
%\hoffset 0in

%\section{Title Page}
\title{littemplate - Literate nuweb C}
\date{CHANGE DATE -- \today}
\author{Michael J. Rossi}

\maketitle


\newpage
\section{Table of Contents}
% Rename contentsname to empty, because it's redundant and we want it
% to show up as a section in the PDF.
\renewcommand*\contentsname{}
\tableofcontents



\newpage
\section{Introduction to littemplate}
\label{sec:introduction}
\boldred{MODIFY ME: QUICK INTRO ABOUT THE PROGRAM.} This is a literate
programming template. You'll modify this to
quickly create a cross platform literate C project with glib.

\subsection{Details About the Code}
littemplate is a
\href{https://en.wikipedia.org/wiki/Literate_programming}{literate 
  program} for \href{http://nuweb.sourceforge.net/}{nuweb} in C.  If
you're not familiar with literate programming, please see the
\hyperref[sec:literate-programming]{Appendix C}.  At the time of
writing, the entire code repository for this program is located
\boldred{HERE}.



\newpage
\section{Building}
\label{sec:building}
The following section describes what you'll need to build and
distribute the code.

\subsection{Dependencies}
\label{sec:dependencies}
If you're going to alter any of the web files (.w), if you want to
re-compile the documentation, or if you stumble across this in the far
future detached from a git repository and nothing but this or the
webs, you'll need
\href{http://nuweb.sourceforge.net/}{nuweb} and
\href{https://www.latex-project.org/}{\LaTeX} to do the tangling
and weaving.

But assuming you're pulling the code from a git repository, you won't
need to actually need to run nuweb or \LaTeX\hspace{0.mm} because the
code has already been separated in the repo.  Therefore on POSIX
systems (including \href{https://www.msys2.org/}{MSYS2} or
\href{https://www.cygwin.com/}{Cygwin} on Windows), you'll only
need the following to compile the code.
\href{https://gcc.gnu.org/}{GCC},
\href{http://www.gnu.org/software/make/}{make}, and
\href{https://wiki.gnome.org/Projects/GLib}{glib} for all the
``batteries'' not found in the standard C 
library.  On Windows, you can try to compile this using either of the
POSIX systems mentioned above or else try using Conan, Cmake, and
Visual Studio.  In either
case, \emph{Glib} was chosen because it is permissive, stable,
and it works on both POSIX and Windows systems.

\boldred{MODIFY ME WITH ADDITIONAL DEPENDENCIES!}

\subsection{Building the Code}
\label{sec:building-the-code}
We provide two builds, a standard POSIX
\href{https://en.wikipedia.org/wiki/GNU_Autotools}{GNU Autotools}
build, and an
alternate build with \href{https://conan.io/}{Conan} and
\href{https://cmake.org/}{Cmake}. The reason we also include a
non-POSIX build system in the form of 
Conan and Cmake is because they work cross platform (including
on Windows using Visual Studio).  But we don't default to this build
because Conan will download many sources that you probably already
have in a POSIX system.  In fact, we normally use
\href{https://www.msys2.org/}{MSYS2} on Microsoft Windows
which also should be able to compile this with no problems.  So the
alternate build is included as an alternate which should work
anywhere, but might not be optimal to the system it's being built on.

But normally if you're in a POSIX environment and if all the dependencies
are installed, you'd simply run the usual \textbf{./configure},
\textbf{make} and optionally, \textbf{make install}. If you're
modifying the code, you'll want to use \textbf{./configure
  --enable-debug} to include the debugging switches.

Otherwise if you'd like to use Conan and Cmake, you'd do the
following:
On a POSIX system you'd run \textbf{make -f Makefile.conan
conan} to use Conan and
Cmake.  On Windows, you'd use the included \textbf{make\_vc.bat}
to do the same thing.

We do include \href{https://www.doxygen.nl/index.html}{Doxygen} style
comments to functions to allow us
to create a familiar HTML interface for high level documentation. But
unless you want to build these, you don't need \emph{Doxygen}
installed.

\subsection{Distribution}
\label{sec:distribution}
The code base will use \emph{git} for version control. Before pushing,
you should run \textbf{make clean-doc}, which will produce the PDF
documentation and the source files necessary to compile the code
without having nuweb or \LaTeX\hspace{0.4mm} installed.  This is the
preferred way. 

Alternatively, or if you'd like a bare minimum of files, run
\textbf{make just-web}, which will reduce everything down to only the
web (.w) files.  This will require the user to weave/tangle the code
with nuweb and \LaTeX and generate the autotools build with
\textbf{autoreconf -iv}. You'll need autotools installed to perform
this last step.



\newpage
\section{Testing}
\label{sec:tests-main}
Before writing any ``actual'' code, we'd like to setup our unit
tests.  We will be using the testing suite in Glib, which requires a
bit of boilerplate in order to create a testing framework.  As we
progress in the code, we will then add tests to this section.  The
tests not only give us confidence when refactoring, but they also help
to document the proper use of functions.

\subsection{Setting Up Tests}
\label{sec:setting-up-tests}
@o all_tests.c @{
@< Warning: Do not modify source directly @>

#include <stdio.h>
#include <string.h>
#include <glib.h>
@< More test headers @>

@< Global Test data @>

@< Test Functions @>

int main(int argc, char **argv)
{
  g_test_init(&argc, &argv, NULL);
  @< Add more tests @>
  return g_test_run();
}
@|@}

\subsection{License and Disclaimer}
\label{sec:license-and-disclaimer}  
Since we don't want anyone changing the ``tangled'' source code
directly, we add a disclaimer at the top.
We'll add this same warning to all of the code.  While we're at it,
we'll also include our license text and generate a COPYING file with
GNU Autotools requires.

@o COPYING @{
@< MIT License @>
@|@}

@d MIT License @{
Copyright 2020 Michael J. Rossi

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
@|@}

@d Warning: Do not modify source directly @{
/************************************************************************
@< MIT License @>
__        ___    ____  _   _ ___ _   _  ____ 
\ \      / / \  |  _ \| \ | |_ _| \ | |/ ___|
 \ \ /\ / / _ \ | |_) |  \| || ||  \| | |  _ 
  \ V  V / ___ \|  _ <| |\  || || |\  | |_| |
   \_/\_/_/   \_\_| \_\_| \_|___|_| \_|\____|
                                             
   This file was a generated by a nuweb literate program.
   DO NOT modify this file directly, 
   but update the appropriate web (.w) file 
   to make code changes.

*************************************************************************/
@|@}


\label{sec:example-of-example}
Since this looks a bit clobbered in a non-fixed font, here is what it
looks like (without the license) in a
fixed font--though it still may not look right if you're reading this
in some other form than a PDF.  It also serves as an example of what
an example looks like in this document.


% This is an example of an example.
\begin{lstlisting}[style=example]
__        ___    ____  _   _ ___ _   _  ____ 
\ \      / / \  |  _ \| \ | |_ _| \ | |/ ___|
 \ \ /\ / / _ \ | |_) |  \| || ||  \| | |  _ 
  \ V  V / ___ \|  _ <| |\  || || |\  | |_| |
   \_/\_/_/   \_\_| \_\_| \_|___|_| \_|\____|
                                             
   This file was a generated by a nuweb literate program.
   DO NOT modify this file directly,
   but update the appropriate web (.w) file
   to make code changes.
\end{lstlisting}

@d Global Test data @{
// None yet.
@|@}


\newpage
\section{The Library}
\label{sec:library-main}
Now we work on the main library, which consists of a header interface
and a library body.

\subsection{mylibrary.h}
\label{sec:mylibrary-h}
Here's the header for our library.

@o mylibrary.h @{
@<Warning: Do not modify source directly@>

#ifndef MYLIBRARY_H
#define MYLIBRARY_H

@< mylibrary spec includes @>
@< mylibrary specs@>

#endif // MYLIBRARY_H
@|@}

@d mylibrary spec includes @{
// None yet.
@|@}

@d mylibrary specs @{
/** Doxygen comment here. This function doubles a number
 * \param n Number to double.
 * \return n doubled.
 */
int doubler(int n);
@|@}

Remember to add it to our test headers!

@d More test headers @{
#include "mylibrary.h"
@|@}

\subsection{mylibrary.c}
\label{sec:mylibrary-c}
Here's our main library body.

@o mylibrary.c @{
@<Warning: Do not modify source directly@>
@< mylibrary body includes @>
@< mylibrary body globals @>
@< mylibrary functions @>
@|@}

@d mylibrary body includes @{
#include <stdio.h>
#include <stdlib.h>
@|@}

@d mylibrary body globals @{
// None yet.
@|@}

\subsection{Doing stuff}
\label{sec:stuff}
Here's what it looks like to add new functions to the program.
But first, let's test it!

@d Test Functions @{
void check_doubler()
{
  g_assert(doubler(10)==20);
}
@| check_doubler @}

To which we add the test.

@d Add more tests @{
g_test_add_func("/set1/doubler test", check_doubler);
@|@}

This function simply doubles a number.
printing.

@d mylibrary functions @{
int doubler(int n)
{
  return n * 2;
}
@| doubler @}



\newpage
\section{The Main Program}
\label{sec:main-program}
\subsection{main.c}
\label{sec:main-c}
Here's our outline.

@o main.c @{
@< Warning: Do not modify source directly @>
@< Main includes @>
@< Main functions @>
@< Main body @>
@|@}

Here's our includes.

@d Main includes @{
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include "mylibrary.h"
  @|@}

Here's a helper function in main to free memory.

@d Main functions @{

void free_gstring (gpointer data, gpointer user_data)
{
  g_string_free(data, TRUE);
}
@| free_gstring @}

\subsection{Main body}
\label{sec:main-body}
Here's the body. For this example, we just test out the ``doubler''
function and some Glib data
structures. Normally you'll want to have this broken apart further\ldots.

@d Main body @{
int main(int argc, char **argv)
{
  // Try out doubler and show some symbols
  int num = doubler(100);
  if(num > 0 && num < 1000)  // note the logical "and" symbol
    printf("100 * 2 = %d\n", num);
  num = doubler(5);
  if(num > 5 || num < 11)  // note the logical "or" symbol
    printf("5 * 2 = %d\n", num);
  if(num != 0)  // note the "not equal" symbol
    puts("Num is positive");
  num = ! 0;  // note the logical "not" symbol
  printf("num: %d\n", num);
  
  // Create a string and a pointer array to hold the strings.
  GString *s = g_string_new("Samantha Bianca");
  GPtrArray *vec = g_ptr_array_new();

  g_ptr_array_add(vec, g_string_new(s->str));
  printf("Ratta: %s\n", s->str);

  // Add to the string and array/vector
  g_string_append(s, " Beatrice");
  g_ptr_array_add(vec, g_string_new(s->str));
  printf("Ratta: %s\n", s->str);

  // Print out the array
  puts("All String versions =>");
  for(guint i = 0; i < vec->len; i++) {
    GString *tmp_str = g_ptr_array_index(vec, i);
    printf("  Vec[%d]: %s\n", i, tmp_str->str);
  }

  // Free the string each string in the array, and the array
  g_string_free(s, TRUE);
  g_ptr_array_foreach(vec, &free_gstring, NULL);  // note the "NULL" symbol
  g_ptr_array_free(vec, TRUE);

  return EXIT_SUCCESS;
}
@|@}

If you need to create a new .w file, you can link it with \emph{@@i somefile.w}.



\newpage
\section{Looking Forward and Backward}
\label{sec:future-and-past}
As the program progress, this section will record important known
issues which we'd like to address.  It also allows us to record major
changes to the code in new releases at a high level.

\subsection{To Do}
\label{sec:todo}
The following are known \emph{TODO} items that we hope to resolve in the
future.

\begin{itemize}
\item Continue to add syntax highlights for Glib types.
\item DO SOMETHING\ldots
\end{itemize}

\subsection{Major Change Log}
\label{sec:major-changlog}
We don't attempt the document every code change here.  That's what the
RCS and code repository are for.  However, we do want to include a
section documenting the major releases and changes here.

\begin{itemize}
\item \textbf{SOME DATE:} Did something\ldots
\end{itemize}



\newpage
\section{Appendix A: Build Files}
\label{sec:build-files}
Here we include the content used to build the program.
We provide two ways to build the project. For Unix/POSIX types, we
provide 
the standard GNU Autotools.  This might even work on Cygwin or MSYS2.
For Windows, we also include the tools necessary to use Visual Studio
via Conan and Cmake.


\subsection{README}
\label{sec:readme}
We produce a simple readme in Markdown format which directs the user
to the literate program for more info.

@o README @{
# littemplate

This is a literate program written in nuweb and C.

Please see littemplate.pdf for the documentation.

## MODIFY ME AND REMOVE THIS SECTION
This is a literate template to quickly generate a cross plaform
C Nuweb project with glib.

Please do the following to refactor the template where littemplate.w
is the name of your actual build.  You'll also want to change the
library name from 'mylibrary' to something else like so:

```
sed -i 's/littemplate/newproject/g' *
sed -i 's/mylibrary/libraryname/g' *
sed -i 's/MYLIBRARY/MYLIBRARY/g' *
mv littemplate.w newproject.w
```

Then run

```
nuweb -rl newproject.w
```

To generate autotools:

```
autoreconf -iv
./configure
make tangle-all
make check
sudo make install
```

This will build all the source files and weaved documentation in
addition to building the code.

To generate Conan & Cmake:

On Windows:
```
mkdir build
make_vc.bat
```

On Unix/POSIX:
```
mkdir build
make -f Makefile.conan
```


@|@}

\subsection{Building With Autotools}
\label{sec:autotools-main}
Here we generate all the files necessary to build using GNU
Autotools.

\subsubsection{Autotools Boilerplate Files}
\label{sec:autotools-boilterplate}
Now we need to add a bunch of boilerplate for Autotools.  Besides a
README, it also requires NEWS, AUTHORS, and ChangeLog.

@o NEWS @{
None yet.
@|@}

@o AUTHORS @{
Michael J. Rossi
@|@}

@o ChangeLog @{
Please see littemplate.pdf for the change log.
@|@}

\newpage
\lstset{style=Make}
\subsubsection{Makefile.am}
\label{sec:makefile}
Here's the raw Makefile.am for this program.

@o Makefile.am -t @{
# Main setup
WEB = littemplate
bin_PROGRAMS=littemplate
littemplate_SOURCES=mylibrary.c, all_tests.c, main.c
littemplate_LDADD=$(GLIB_LIBS) mylibrary.o

# set these based on what we want
if DEBUG
littemplate_CFLAGS=$(MY_DEBUG_FLAGS) $(GLIB_CFLAGS)
else
#littemplate_CFLAGS=$(MY_RELEASE_FLAGS) $(GLIB_CFLAGS)
littemplate_CFLAGS=$(GLIB_CFLAGS)
endif

# Unit tests
TESTS=$(check_PROGRAMS)
check_PROGRAMS=all_tests
all_tests_CFLAGS=$(MY_DEBUG_FLAGS) $(GLIB_CFLAGS)
all_tests_LDADD=$(GLIB_LIBS) mylibrary.o

# Nuweb specific
tangle:
	nuweb -rl $(WEB).w

weave: tangle
	pdflatex '\scrollmode \input $(WEB).tex'
	nuweb -rl $(WEB).w
	pdflatex '\scrollmode \input $(WEB).tex'

tangle-all: tangle weave all

# Dev tools
run:
	./$(WEB)

trun: tangle run

tcrun: tangle check run

# Doc tools
clean-doc: clean distclean
	rm -f *.log *.aux *.tex *.out *.dvi *.toc

dox:
	doxygen -g
	doxygen Doxyfile

clean-dox:
	rm -rf Doxyfile html latex

fresh: clean
	rm -rf *.pdf *.c *.h

# delete everything but the webs and hidden folders
just-web: clean-doc fresh clean-dox
	rm -rf autom4te.cache build
	find . ! -name *.w -type f -exec rm -f {} +
@|@}

\subsubsection{configure.ac}
\label{sec:configure}
Here's our autoconf configure file.

@o configure.ac @{

# -*- Autoconf -*-
# My C boilerplate

# Main setup
AC_PREREQ([2.69])
AC_INIT([littemplate], [0.0.1], [dionysius.rossi@@gmail.com])
AC_CONFIG_SRCDIR([config.h.in])
AC_CONFIG_HEADERS([config.h])
AM_INIT_AUTOMAKE

# check if the source folder is available
#AC_CONFIG_SRCDIR([src/littemplate.c])

# C specific
AC_PROG_CC
AC_PROG_CC_STDC

# Enable GCC/Clang debug flags from ./configure --enable-debug
AC_ARG_ENABLE(debug,
AS_HELP_STRING([--enable-debug],
               [enable debugging, default: no]),
[case "${enableval}" in
             yes) debug=true ;;
             no)  debug=false ;;
             *)   AC_MSG_ERROR([bad value ${enableval} for --enable-debug]) ;;
esac],
[debug=false])
AM_CONDITIONAL(DEBUG, test x"$debug" = x"true")

# GCC / Clang specific flags
AC_SUBST([MY_DEBUG_FLAGS],
  ['-g3 -Og -Wall -Werror -pedantic -std=c17'])
AC_SUBST([MY_RELEASE_FLAGS],
  ['-D_FORTIFY_SOURCE=2 -fpie -Wl,-pie -O2 -Wall -Werror -pedantic -std=c17'])

# Checks for libraries.
GLIB_REQUIRED_VERSION=2.0.0
AM_PATH_GLIB_2_0($GLIB_REQUIRED_VERSION, ,
  [AC_MSG_ERROR(Can't find glib.
  Please make sure that libglib, libglib-dev and pkg-config are installed)],
  gobject)

# Gtk+
#GTK_REQUIRED_VERSION=3.0.0
#AM_PATH_GTK_3_0($GTK_REQUIRED_VERSION, ,
#  [AC_MSG_ERROR(Test for Gtk+ 3 failed)], gobject)

# Checks for header files.
AC_CHECK_HEADERS([stdlib.h])


# Final required
AC_CONFIG_FILES([Makefile])
AC_OUTPUT
@|@}


\subsection{Building With Conan and Cmake}
\label{sec:visual-studio-main}
Here we include the build files to use Conan and Cmake, which should
even work on Microsoft Windows.  You should also be able to use this on
POSIX systems, but since Conan will likely re-build many libraries you
already have, you're probably better off using Autotools.

\subsubsection{Conan}
Here is the file necessary to build with Conan.

@o conanfile.txt @{
[requires]
glib/2.65.1

[generators]
cmake

[options]
glib:shared=True

[imports]
# Copies all dll files from packages to bin folder
bin, *.dll -> ./bin
@|@}

\subsubsection{Cmake}
Here is the Cmake file for conan.  It is very basic.

@o CMakeLists.txt @{cmake_minimum_required( VERSION 3.10 )
project( littemplate VERSION 1.0 )

include( ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake )
conan_basic_setup()

add_executable( littemplate main.c mylibrary.c )
target_link_libraries( littemplate ${CONAN_LIBS} )
add_executable( all_tests all_tests.c mylibrary.c )
target_link_libraries( all_tests ${CONAN_LIBS} )

# Or if static
#target_link_libraries( littemplate ${CONAN_LIBS} -static )
@|@}

\subsubsection{Make Batch Script}
The following is a simple Windows batch file to build with.
It obviously
assumes you already have the Visual C compiler, conan, and cmake
installed. \textbf{Please make
  sure a directory named ``build'' already exists.}

@o make_vc.bat @{
@@echo off
cd build
conan install .. --build=missing
cmake .. -G "Visual Studio 15" -A x64
cmake --build . --config Release
cd ..
@|@}

\subsubsection{GCC Conan Makefile}
Here's how you might build via a GNU Makefile to
build with GCC and conan and Cmake.  So a \textbf{make -f Makefile.conan
conan} should do the trick. \textbf{Please make
  sure a directory named ``build'' already exists.}

@o Makefile.conan -t @{
conan:
	cd build; conan install .. --build=missing ; \
	cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release; cmake --build .

cmake:
	cd build ; cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release; cmake --build .
@|@}



%Go back to normal style
\lstset{style=CWEB C}

\newpage
\section{Appendix B: Final C Source Files}
\label{sec:final-sources-main}
Here is the actual weaved C source code. Other then the line numbers
(which we've added), this is the raw C source code.  Although Donald
Knuth believes that the produced code should be ``tangled'' so that
people won't be tempted to modify the source and force them to
read the ``weaved'' documentation, we choose not to follow this path.
Instead, we want the option of including \emph{Doxygen} comments and
we want the raw produced code.
We do include a disclaimer at the top of each file, but
with the advent of places like \emph{github} and tools like
\emph{Doxygen}, we believe it's better to include the actual code for
quick perusal in a site like \emph{github}.  Additionally,
\emph{Doxygen} allows us to also create a separate quick reference
interface when we just need to quickly find a function once we
understand the code base.

Although the literate program should be the main source of info, we
find these additional tools to be extremely useful as alternate ways
to understand the code.

\subsection{mylibrary.h}
\label{sec:final-mylibrary-h}
\includenumberedtextfile{mylibrary.h}

\subsection{mylibrary.c}
\label{sec:final-mylibrary-c}
\includenumberedtextfile{mylibrary.c}

\subsection{main.c}
\label{sec:final-main-c}
\includenumberedtextfile{main.c}

\subsection{all\_tests.c}
\label{sec:all-tests-c}
\includenumberedtextfile{all_tests.c}



\newpage
\section{Appendix C: Literate Programming}
\label{sec:literate-programming}
The following section describes what literate programming is and why
this code is written in this style.  Feel free to skip this if you
know what this is and don't care about the details of it.

\subsection{What is Literate Programming?}
\label{sec:what-is-literate-programming}
If you're not familiar with literate programming, it is essentially a
way to write a program in the same way that you might write an article
explaining the details of a program in an online article, ebook, or
even a book made from an actual dead tree. In these
presentations, you would
typically explain some things in some formatted text and then present
the actual code.  This might be followed by more text and then more
code. 
This is essentially what you do in a literate program.  You combine
formatted prose and actual program code at the same time \emph{in any
  order}
that best helps you reach your goal (which is typically to explain the
program to someone else or yourself in the future).

More specifically, a literate program is a text file that
combines both prose (in this case, formatted in
\href{https://www.latex-project.org/}\LaTeX) and the program
source code mixed into a single file called a ``web''
(using the extension \emph{.w}).  The
literate programming tool in our case,
\href{http://nuweb.sourceforge.net/}{nuweb}, parses the web file and
joins all 
the code snippets into \hyperref[sec:final-sources-main]{actual
  working code files} (called
``tangling'') while also generating
this fancy document you're reading (called ``weaving'').
You will have to arrange an outline of the code in a way that your
compiler or interpreter requires, but you can fill in those details in
any order you like.  Once you run nuweb, it will \emph{tangle} the
code in the
order that the programming language requires that you specified in the
outline. This gives you the
freedom to
write the program in whatever order you think is best, be it top down,
bottom up, or even something else. This makes
it easy for someone to understand the who, what, wheres, whys, and
hows of a program.

But beyond just
creating nice and elaborate documentation of a program, literate
programming also has
other advantages.  It allows you to break apart large
chunks of code in creative ways--a ``chunk'' being an arbitrary group
of code that will be \emph{tangled} into the final code.  So if you
have a
very large or very complicated function, you can write each part of
the function as a separate chunk of code, describing what's going on
in between chunks,
and let nuweb take care of joining them all together into the
final product.  In the same way, you can also use this feature to
handle complex code that drifts so far to the right it's in danger of
falling off the page.  For situations like this, you can simply break
each indented part into a separate chunk, describe each, and still
have them written legibly and still producing valid code in the
final product.  Finally,
since you can put chunks of
code anywhere in the web file, you can use nuweb to reduce the
inevitable boiler
plate we're all forced to write by simply reusing chunks of code we've
written once as a kind of handy meta macro system.

Please keep in mind that all the code does not have to be in a massive
single
web (\emph{.w}) file, though it certainly can for smaller programs.
You can have a main web file and simply include
the other separate web files into the main web to generate the final
code and documentation.  This allows teams to work on code and of
course, saves us from having to search through a single massive file
for something.

Although many literate programming tools exist, I chose nuweb
because I believe that it has a number of advantages over other
general literate programming tools.  It uses \LaTeX, and not \TeX,
which has more capabilities in my opinion.  I also like that it's
programming language agnostic, so I can use a single template for
Common Lisp, C,
Ada, or anything else I wanted to hack in.  Finally, I like that it
allows me to include comments in the code and does not force the code
to be ``tangled'' like in the original \emph{WEB} and \emph{CWEB} of
St. Donald Knuth--where ``tangle'' meant that the final code was
purposely made ugly and comment free so that readers are not likely to
read or modify it.  I prefer to have comments left in the code and for
the code to be actually legible, which is what nuweb does.
Having clean code and comments in the tangled code also allows me to
put
\href{https://www.doxygen.nl/index.html}{Doxygen} comments in the
code which can be used to create a handy high level interface when we
just need to remember how to call a function, and don't need the whole
story of what we did to implement it and why.

Finally, I feel compelled to mention that I did not write this
appendix to try
to get you to join the
literate programming cult (though that's nifty if you do).  I wrote it
simply to
explain the who, whats, whys, and hows of this program--see what I did
there\ldots.

\subsection{Reading a Web}
\label{sec:cweb-formatting}
Reading a weaved nuweb literate program should be fairly intuitive.
There are two types of code chunks in nuweb.  A ``file chunk'' and an
``fragment chunk.''

A file chunk is where you tell nuweb that you
want to create a new file with the following name and extension.  This
will then be followed by the actual code or else links to fragment
chunks of code that you will fill in later. A file chunk is where
you'll normally setup the broad outline of the program that will be
filled out by the explicitly mentioned fragment chunks, and possibly
other arbitrary fragment chunks in the fragment chunks themselves.

A fragment chunk is just that.  It's a plain text identifier which
links to a piece of code somewhere else in the web.  It can itself
contain other arbitrary fragment chunks which will then need to be
filled in wherever the author decides to.

So when you run into a chunk of code, it will begin with either the
name 
of the file that will be written (which means it's a file chunk) or
some bracketed arbitrary text that was
specified in a file chunk or previous fragment chunk (which means it's
a fragment chunk).  A ``letter number'' (like
``7a'') will follow each fragment chunk. This refers to the actual
page
number in the document where you'll find the fragment where the code
will be elaborated.  If
you're reading this on a viewer that properly handles links in
documents, you should be able to click on the ``letter number''
following the chunk and it will jump to that section.

Following a chunk of code, nuweb might also produce some other data
pointing you to other places where the code is referenced and linking
to certain functions or variables within the web that the author has
chosen to mark.  Finally, nuweb
generates an index for us at the end of this document which includes a
lists of all the file and fragment chunks of code.  That's about it
when it comes to the layout.

Regarding the code listing style, this particular web tries to emulate
the \TeX\hspace{0.5mm} style of Donald
Knuth's \href{https://en.wikipedia.org/wiki/CWEB}{CWEB}.
Although I include \LaTeX\hspace{0.4mm} styles that allow a
plain old boring ``mono'' font, I like the fancy look of the formatted
\emph{CWEB} code.  This means that certain things like ``=='' actually
look like this: ``\(\equiv\)'' in this document.  For the most part,
the symbols come from
\href{https://en.wikipedia.org/wiki/List_of_mathematical_symbols}
{mathematics}
and most are self-explanatory.  But some
symbols like \(\equiv\),
might require a quick explanation.  The other symbols which
might be confusing are the logical symbols which come from
mathematics.
``\(\land\)'', is equivalent to ``\&\&'' (aka ``and'') in C.
``\(\lor\)'' is equivalent to ``\(\mid \mid\)'' (aka ``or'') in C.
``\(\not=\)'', is equivalent to ``!='' (not in C).
``\(\neg\)'', is equivalent to logical ``!'' (not in C).
``\(\Lambda\)'' is equivalent to ``NULL'' (as in a NULL pointer).
Finally, a ``\textvisiblespace'' is used to show spaces in strings.
I.e. ``Bob\textvisiblespace ran\textvisiblespace home.''
If you're ever confused about a symbol, you can
always check the 
\hyperref[sec:final-sources-main]{raw code listings} at the end of the
document to see what something \emph{tangles} into.

If you still can't adapt to this format, you can always
re-\emph{weave}
the document
by changing \begin{verbatim}\lstset{style=CWEB C}\end{verbatim}
to \begin{verbatim}\lstset{style=Mono C}\end{verbatim} in the main web
file (near the top of the file).  You can also comment and un-comment
with a \%. Then you can re-weave and this document will produce plain,
clear, simple, (and boring) C code.

\subsection{This Particular nuweb}
\label{sec:this-nuweb}
In the case of this program, nuweb will be used to produce the entire
project, including the build and secondary files.
In other words, the
base program consists of \hyperref[sec:raw-nuweb]{one or more web
  (\emph{.w}) files}.  Everything else is generated by nuweb from the
web files.  Simply run \textbf{nuweb -rl littemplate.w} on the main
web, and all the necessary source code and build code will be
generated.

By default, we generate a PDF for the documentation.  But once nuweb
generates a .tex file, you can convert it to other format's like HTML
or even an ebook.  For example, you can run
\textbf{tex4ebook -f mobi littemplate.tex} to generate a Kindle
version. This also first generates pretty good looking HTML
first. There's plenty of other options out there if you're not
satisfied.  Let your search engine guide you.



\newpage
\section{Appendix D: Raw nuweb Files}
\label{sec:raw-nuweb}
Here are the raw nuweb files that produced this.  The only thing not
included in this is the Makefile (included above). How meta is it to
include the source in the source? But in all seriousness, this allows
folks to view the raw final web in it's raw state.
%\subsection{littemplate.w}
\includetextfile{littemplate.w}


\newpage
\section{Index}
\label{sec:indices}

{\bf nuweb} can automatically create three sets of indices: an index
of file
names, an index of fragment names, and an index of user-specified
identifiers. An index entry includes the name of the entry, where it
was defined, and where it was referenced.

\subsection{Generated Files}
\label{sec:index-files}
@f

\subsection{Identifiers}
\label{sec:index-identifiers}
{\bf nuweb} allows us to print out our identifiers in single column
format, instead of forcing you to use a two-column format like
{\bf CWEB}.
We could force this automatically by emitting the \verb|\twocolumn|
command, but this will force a new page. Since we don't have enough
identifiers to justify this, we'll just keep it in one column.

@u

\subsection{Fragments}
\label{sec:index-fragments}

@m

\end{document}
